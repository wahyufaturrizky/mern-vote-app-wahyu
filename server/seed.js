const db = require('./models');

const users = [
	{
		username: 'username',
		password: 'password',
	},
	{
		username: 'admin',
		password: '123456',
	},
];

const polls = [
	{
		question: 'Lorem ipsum dolor',
		options: ['React', 'Angular', 'VueJs'],
	},
	{
		question: 'Lorem ipsum dolor',
		options: ['React', 'Angular', 'VueJs'],
	},
	{
		question: 'Lorem ipsum dolor',
		options: ['React', 'Angular', 'VueJs'],
	},
];

const seed = async () => {
	try {
		await db.User.remove();
		console.log('DROP ALL USER');

		await db.Poll.remove();
		console.log('DROP ALL POLLS');

		await Promise.all(
			users.map(async (user) => {
				const data = await db.User.create(user);
				await data.save();
			})
		);

		console.log('CREATED USERS', JSON.stringify(users));

		await Promise.all(
			polls.map(async (poll) => {
				poll.options = poll.options.map((option) => ({ option, vote: 0 }));

				const data = await db.Poll.create(poll);
				const user = await db.User.findOne({ username: 'username' });
				data.user = user;
				user.polls.push(data._id);
				await user.save();
				await user.save();
			})
		);

		console.log('CREATED POLLS', JSON.stringify(polls));
	} catch (err) {
		console.error(err);
	}
};

seed();

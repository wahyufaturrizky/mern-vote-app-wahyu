require('dotenv').config();

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const routes = require('./routes');
const { errorHandler, notFoundHandler } = require('./handlers');
const app = express();
const port = process.env.PORT;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) =>
	res.json({
		message: `server is running on port = ${port} create by https://www.linkedin.com/in/wahyu-fatur-rizky/`,
		status: 200,
	})
);

app.use('/api/auth', routes.auth);

app.use(notFoundHandler);

app.use(errorHandler);

app.listen(
	port,
	console.log(
		`Post running on ${port} create by https://www.linkedin.com/in/wahyu-fatur-rizky/`
	)
);

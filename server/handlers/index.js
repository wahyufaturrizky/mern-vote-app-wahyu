module.exports = {
	...require('./auth'),
};

module.exports.errorHandler = (err, req, res, next) => {
	res.status(err.status || 500).json({
		err: err.message || 'Something went wrong',
		status: err.status || 500,
	});
};

module.exports.notFoundHandler = (req, res, next) => {
	const err = new Error('Not found');
	err.status = 404;

	next(err);
};
